<h1 align="center">Rest Countries</h1>

<div align="center">
  <h3>
    <a href="https://hariprakashcountriesproject.netlify.app/">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/hariprakash20/rest-countries">
      Solution
    </a>
  </h3>
</div>

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Overview](#overview)
  - [Built With](#built-with)
- [How To Use](#how-to-use)
- [Contact](#contact)

## Overview

![](src/assets/Output.png)

### Built With

```
$ npm create vite@latest
```


## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone git@gitlab.com:hariprakash20/rest-countries.git


# Install dependencies
$ npm install

# Run the app
$ npm run dev
```

## Contact

- Gitlab [@hariprakash20](https://gitlab.com/hariprakash20)

