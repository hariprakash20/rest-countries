import React, {useState, useEffect} from 'react'
import Navbar from './components/Navbar'
import MainContent from './components/MainContent'
import { Route, Routes} from 'react-router-dom'
import { getRawCountriesData } from './services/restCountriesAPI'
import EachCountryDetails from './components/EachCountryDetails'


function App() {
  
  const [rawData, setRawData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getRawCountriesData().then(response => setRawData(response));
    setIsLoading(false);
  },[])

  let countriesData = rawData.map((country) => {
    return {
      name: country.name.common,
      population: country.population,
      region: country.region,
      capital: country.capital && country.capital[0],
      flag: country.flags.png,
      subRegion: country.subregion,
      area: country.area,
      cca3: country.cca3
    };
  });

return (
    <div className="App">
      <Navbar />
      {/* <MainContent countriesData={countriesData} /> */}
      <Routes>
        <Route path='/' element={<MainContent countriesData={countriesData} isLoading={isLoading} />} />
        <Route path="/:id" element={<EachCountryDetails countriesData={countriesData}/>}/>
      </Routes>
    </div>
  )
}

export default App
