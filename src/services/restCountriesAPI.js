import axios from "axios"

const countriesClient = axios.create({
    baseURL:'https://restcountries.com/v3.1/',
})

export function getRawCountriesData(){
    return countriesClient.get('/all').then(response => response.data)
    
}