import React from "react";
import { Link } from "react-router-dom";

export default function CountryTile({
  name,
  population,
  region,
  capital,
  flag,
  subRegion,
  area,
  cca3,
}) {
  return (
    <Link to={`${cca3}`}>
      <div className="border w-20 m-3 d-flex flex-column align-items-center rounded-3 pointer">
        <img src={flag} alt="flag" className="rounded-3" />
        <p>
          <b>Name:</b> {name}
        </p>
        <p>
          <b>Population:</b> {population}
        </p>
        <p>
          <b>Region:</b> {region}
        </p>
        <p>
          <b>Capital:</b> {capital}
        </p>
        <p>
          <b>Sub-region:</b> {subRegion}
        </p>
        <p>
          <b>Area:</b> {area}
        </p>
      </div>
    </Link>
  );
}
