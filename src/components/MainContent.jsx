import React, { useState } from "react";
import CountryTile from "./CountryTile";
import getSubRegions from "../getSubRegions";
import SearchBar from "./SearchBar";
import DropDownMenu from "./DropDownMenu";


export default function MainContent({ countriesData, isLoading }) {
  const [region, setRegion] = useState("");
  const [subRegion, setSubRegion] = useState("");
  const [sortBy, setSortBy] = useState("none");
  const [searchString, setSearchString] = useState("");
  const [isAssending, setIsAssending] = useState(true);
  

  let regions = [...new Set(countriesData.map((country) => country.region))];

  let subRegions = getSubRegions(countriesData, regions);

  let subRegionsList = [];

  if (region) subRegionsList = Object.keys(subRegions[region]);

  function changeSubRegion(event) {
    setSubRegion(event.target.value);
  }

  function changeRegion(event) {
    setRegion(event.target.value);
    setSubRegion("");
  }

  function updatePage(event) {
    setSearchString(event.target.value);
  }

  function changeSortBy(event) {
    setSortBy(event.target.value);
  }

  function changeOrder(event) {
    event.target.value == "Ascending"
      ? setIsAssending(true)
      : setIsAssending(false);
  }

  if (sortBy !== "none") {
    if (isAssending)
      countriesData.sort((a, b) => (a[sortBy] > b[sortBy] ? 1 : -1));
    else {
      countriesData.sort((a, b) => (a[sortBy] > b[sortBy] ? -1 : 1));
    }
  }

  let allCountries = countriesData.map((country, index) => {
    if (subRegion !== "" && country.subRegion !== subRegion) {
      return;
    } else if (region !== "" && country.region !== region) {
      return;
    } else if (
      !country.name.toLowerCase().startsWith(searchString.trim().toLowerCase())
    ) {
      return;
    } else {
      return (
        <CountryTile
          key={index + 1}
          name={country.name}
          population={country.population}
          region={country.region}
          capital={country.capital}
          flag={country.flag}
          subRegion={country.subRegion}
          area={country.area}
          cca3={country.cca3}
        />
      );
    }
  });

allCountries = allCountries.filter(each => each!== undefined);

  return (
    <div className="main-content mt-3 bg-color">
      <div className="d-flex d-flex justify-content-around options">
        <SearchBar updatePage={updatePage} />
        <DropDownMenu
          handleChange={changeRegion}
          options={regions}
          filter="regions"
        />
        <DropDownMenu
          handleChange={changeSubRegion}
          options={subRegionsList}
          filter="subregions"
        />
        <DropDownMenu
          handleChange={changeSortBy}
          options={["name", "area", "population"]}
          filter="sort"
        />
        <DropDownMenu
          handleChange={changeOrder}
          options={["Ascending", "Descending"]}
          filter="order"
        />
      </div>
      <div className="d-flex flex-wrap mt-4">
        {
          allCountries.length != 0 ? 
          (allCountries) :
          (<>
            {
              !isLoading && <h1>No country details found</h1>
            } 
            {(isLoading && <div className="spinner-border" role="status">
            </div>)}
            </>
            )
        }
      </div>
    </div>
  );
}
