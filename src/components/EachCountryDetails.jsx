import React from "react";
import { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";

const EachCountryDetails = ({ countriesData }) => {
  let { id } = useParams();

  const [country, setCountry] = useState(null);
  const [currency, setCurrency] = useState(null);

  let getCountry = async () => {
    fetch(`https://restcountries.com/v3.1/alpha/${id}`)
      .then((res) => res.json())
      .then((res) => {
        setCountry(res);
        setCurrency(Object.keys(res[0].currencies)[0]);
      });
  };

  useEffect(() => {
    getCountry();
  }, []);

  return country ? (
    <>
      <Link to="/">
        <button>back</button>
      </Link>
      <div className="content d-flex justify-content-evenly">
        <img src={country[0].flags.png} alt="flag" className="rounded-3" />
        <div className="column1">
          <div className="detail">
            <b>Native Name:</b> {country[0].name.nativeName ?. [Object.keys(country[0].name.nativeName)[0]].common || "N/A"}
          </div>
          <div className="detail">
            <b>Population:</b> {country[0].population}
          </div>
          <div className="detail">
            <b>Region:</b> {country[0].region}
          </div>
          <div className="detail">
            <b>Sub Region:</b> {country[0].subregion}
          </div>
          <div className="detail">
            <b>Capital:</b> {country[0].capital}
          </div>
        </div>
        <div className="column2">
          <div className="detail">
            <b>Top Level Domain:</b> {country[0].tld}
          </div>
          <div className="detail">
            <b>Currencies:</b> {country[0].currencies[currency].name}
          </div>
          <div className="detail">
            <b>Languages:</b>{" "}
            {Object.values(country[0].languages).map((lang) => lang + " ")}
          </div>
        </div>
      </div>
      <div>
          {country[0].borders?.length ? (<h1>Borders: {((country[0].borders.map(each=> countriesData.filter(border => each === border.cca3)[0].name+" ")))}</h1>) : (<h1>no border</h1>) }
        </div>
    </>
  ) : (
    <h1>no page found</h1>
  );
};

export default EachCountryDetails;
