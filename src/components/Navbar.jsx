import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'

export default function Navbar(){
    return(
        <nav className='navbar navbar-expand-lg navbar-light bg-light shadow d-flex justify-content-between'>
            <h2 className='ms-5'>Where in the world?</h2>
            <button type="button" className="btn btn-dark me-5">Dark</button>
        </nav>   
    )
}