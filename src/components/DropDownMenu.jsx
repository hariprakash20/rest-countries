import React from "react";

export default function DropDownMenu({ handleChange, options ,filter}) {
  let allOptions = options && options.map((option) => (
    <option value={option} key={option}>
      {option}
    </option>
  ));
  return (
    <select
      className="width15 fs-half form-select"
      aria-label="Default select example"
      onChange={handleChange}
    >
      <option value="">{filter}</option>
      {allOptions}
    </select>
  );
}
