import React from 'react';

export default function SearchBar({ updatePage }){
    return(
        <div className="input-group mb-3 width40">
        <input
          type="text"
          className="form-control"
          placeholder="Enter the country"
          onChange={updatePage}
          aria-label="Recipient's username"
          aria-describedby="basic-addon2"
        />
        <span className="input-group-text" id="basic-addon2">
          Search
        </span>
      </div>
    )
}