export default function getSubRegions(countriesData, regions){
    let subRegions = {};
regions.map((region) => (subRegions[region] = {}));
countriesData.map(country => subRegions[country.region][country.subRegion]= 1);
return subRegions;
}
